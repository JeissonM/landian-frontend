import { FC } from 'react';
import { Avatar, Link } from '@mui/material';
import ImageIcon from '@mui/icons-material/Image';
import WorkIcon from '@mui/icons-material/Work';
import BeachAccessIcon from '@mui/icons-material/BeachAccess';
import './style.css';

const NavBar: FC = () => {
  return (
    <>
      <div className="containerNavBar">
        <Avatar className="avatar" sx={{ bgcolor: '#426cdf' }}>
          <ImageIcon />
        </Avatar>
        <Link href="/" underline="none" color="#fff" className="titleLinkNav">
          Home
        </Link>
        <Avatar className="avatar" sx={{ bgcolor: '#426cdf' }}>
          <ImageIcon />
        </Avatar>
        <Link
          href="/users"
          underline="none"
          color="#fff"
          className="titleLinkNav"
        >
          Users
        </Link>
        <Avatar className="avatar" sx={{ bgcolor: '#426cdf' }}>
          <WorkIcon />
        </Avatar>
        <Link
          href="/lands"
          underline="none"
          color="#fff"
          className="titleLinkNav"
        >
          Lands
        </Link>
      </div>
    </>
  );
};

export default NavBar;
