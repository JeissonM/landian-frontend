import { FC } from 'react';
import './style.css';

const Header: FC = () => {
  return (
    <div className="container">
      <h1 className="titleHeader">Landian services</h1>
    </div>
  );
};

export default Header;
