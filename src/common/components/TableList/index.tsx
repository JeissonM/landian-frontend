import {
  Paper,
  Stack,
  Table,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from '@mui/material';
import { FC, ReactNode } from 'react';
import './style.css';
import PlagiarismIcon from '@mui/icons-material/Plagiarism';

interface ITableList {
  title: string;
  description?: string;
  secondDescription?: string;
  children?: ReactNode;
  className?: string | undefined;
}

const TableList: FC<ITableList> = ({
  title,
  description,
  children,
  secondDescription,
  className,
}) => {
  return (
    <>
      <section>
        <Stack spacing={2} justifyContent="center" alignItems="center">
          <div>
            <h1 className="titleUser">
              {title} <PlagiarismIcon fontSize="large" />
            </h1>
          </div>
          <br />
          <br />
          <div>
            <Paper >
              <TableContainer>{children}</TableContainer>
            </Paper>
          </div>
        </Stack>
      </section>
    </>
  );
};

export default TableList;
