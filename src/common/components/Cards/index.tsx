import {
  Card,
  CardActions,
  CardContent,
  Link,
  Typography,
} from '@mui/material';
import { FC } from 'react';
import './style.css';

interface ICard {
  title: string;
  href?: string;
  titleLink?: string;
  description?: string;
  token?: number;
  className?: string;
}

const Cards: FC<ICard> = ({
  title,
  href,
  titleLink,
  description,
  token,
  className,
}) => {
  return (
    <Card className={className || 'containerCard'}>
      <CardContent>
        <h1 className={className || 'titleCard'}>{title}</h1>
        <h3>{description}</h3>
        <h5 className="titleToken">{token}</h5>
      </CardContent>
      <CardActions>
        <Link href={href} underline="none">
          {titleLink}
        </Link>
      </CardActions>
    </Card>
  );
};

export default Cards;
