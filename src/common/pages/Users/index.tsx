import { FC, useCallback, useEffect, useState } from 'react';
import axios from 'axios';
import TableList from '../../components/TableList';
import './style.css';
import {
  Box,
  Button,
  Modal,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
} from '@mui/material';

const Users: FC = () => {
  // estilos modal
  const style = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    minWidth: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
  };
  //modal
  const [open, setOpen] = useState(false);
  const handleModal = () => setOpen(!open);
  //estados
  const [userId, setUserId] = useState<any>();
  const [dataUsers, setDataUsers] = useState<any[]>([]);
  const [dataCantLands, setDataCantLands] = useState(0);
  const [dataLands, setDataLands] = useState<any[]>([]);

  //Listado Users
  //UseEffect #1 consulta user
  const URL = 'http://127.0.0.1:3333/users';
  useEffect(() => {
    const getUsers = async () => {
      const response = await axios.get(URL);
      console.log('Data', response.data.data.data);
      setDataUsers(response.data.data.data);
    };
    getUsers();
  }, []);

  //funcion para llamar cantidad de lands del usuario
  const call = useCallback(() => {
    const getCantLnads = async () => {
      const response = await axios.get(
        `http://127.0.0.1:3333/cant-lands-user/${userId.id}`
      );
      console.log('Cantidad de Lands:', response.data.data);
      setDataCantLands(response.data.data.number_lands);
    };
    getCantLnads();
  }, [userId]);

  let idPerron;

  //funcion de onclick que guarda el id de la fila deusuario
  const handleChange = (id: number) => {
    setUserId({ id });
    idPerron = id;
    console.log('Perron', idPerron);
  };

  //funcion para llamar los lands, solo si e4l usuario tiene
  const callLand = useCallback(() => {
    const getLandsPerUser = async () => {
      const response = await axios.get(
        `http://127.0.0.1:3333/lands-per-user/${userId.id}`
      );
      console.log('Lands del user', response.data.data.lands);
      setDataLands(response.data.data.lands);
    };
    getLandsPerUser();
  }, [userId]);

  return (
    <>
      <TableList title="User List">
        <TableHead>
          <TableRow>
            <TableCell align="center" colSpan={3}>
              <b>Name</b>
            </TableCell>
            <TableCell align="center" colSpan={3}>
              <b>Email</b>
            </TableCell>
            <TableCell align="center" colSpan={3}>
              <b>Details</b>
            </TableCell>
          </TableRow>
        </TableHead>
        {dataUsers.map((dataUser) => (
          <Table key={dataUser.id}>
            <TableBody>
              <TableRow>
                <TableCell
                  align="center"
                  className="tableCellUser"
                  id={dataUser.id}
                >
                  <div>{dataUser.name}</div>
                </TableCell>
                <TableCell align="center" className="tableCellUser">
                  {dataUser.email}
                </TableCell>
                <TableCell align="center" className="tableCellUser">
                  <Button
                    variant="contained"
                    className="hola"
                    onClick={() => {
                      call();
                      handleModal();
                      handleChange(dataUser.id);
                      callLand();
                    }}
                  >
                    Details
                  </Button>
                </TableCell>
              </TableRow>
            </TableBody>
          </Table>
        ))}
      </TableList>
      <Modal
        open={open}
        onClose={handleModal}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography id="modal-modal-title" variant="h6" component="h2">
            Lands del Usuario
          </Typography>
          <Typography id="modal-modal-description" sx={{ mt: 2 }}>
            Cantidad lands: {dataCantLands}
          </Typography>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell align="center" colSpan={3}>
                  <b>Name</b>
                </TableCell>
                <TableCell align="center" colSpan={3}>
                  <b>Tonken</b>
                </TableCell>
              </TableRow>
            </TableHead>
            {dataLands.map((dataland) => (
              <TableBody key={dataland.id}>
                <TableRow>
                  <TableCell align="center" colSpan={2}>
                    {dataland.name}
                  </TableCell>
                  <TableCell align="center" colSpan={2}>
                    {dataland.token_id}
                  </TableCell>
                </TableRow>
              </TableBody>
            ))}
          </Table>
        </Box>
      </Modal>
    </>
  );
};

export default Users;
