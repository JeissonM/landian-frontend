import { FC, useEffect, useState } from 'react';
import TableList from '../../components/TableList';
import './style.css';
import axios from 'axios';
import {
  Pagination,
  Table,
  TableBody,
  TableCell,
  TableRow,
} from '@mui/material';

const Lands: FC = () => {
  const [dataLands, setDataLands] = useState<any[]>([]);
  const [page, setPage] = useState(1);

  useEffect(() => {
    const getLands = async () => {
      const response = await axios.get(
        `http://127.0.0.1:3333/lands?page=${page}`
      );
      setDataLands(response.data.data.data);
    };
    getLands();
  }, [page]);

  const handleChange = (e: React.ChangeEvent<unknown>, value: number) => {
    setPage(value);
  };

  return (
    <>
      <TableList title="Lands List" description="Token">
        {dataLands.map((dataLand) => (
          <Table key={dataLand.id}>
            <TableBody>
              <TableRow>
                <TableCell align="center" className="tableCell">
                  <div>{dataLand.name}</div>
                </TableCell>
                <TableCell align="center" className="tableCell">
                  {dataLand.token_id}
                </TableCell>
              </TableRow>
            </TableBody>
          </Table>
        ))}
        <Pagination
          className="pagination"
          count={2}
          page={page}
          onChange={handleChange}
        />
      </TableList>
    </>
  );
};

export default Lands;
