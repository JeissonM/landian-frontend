import { FC } from 'react';
import Cards from '../../components/Cards';
import './style.css';

const Home: FC = () => {
  return (
    <div className="containerHome">
      <Cards title="Users" href="/users" titleLink="Search" />
      <Cards title="Lands" href="/lands" titleLink="Search" />
    </div>
  );
};

export default Home;
