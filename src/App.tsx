import { FC } from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Header from './common/components/Header';
import NavBar from './common/components/NavBar';
import Home from './common/pages/Home';
import Lands from './common/pages/Lands';
import Users from './common/pages/Users';

const App: FC = () => {
  return (
    <Router>
      <Header />
      <NavBar />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/users" element={<Users />} />
        <Route path="/lands" element={<Lands />} />
      </Routes>
    </Router>
  );
};

export default App;
